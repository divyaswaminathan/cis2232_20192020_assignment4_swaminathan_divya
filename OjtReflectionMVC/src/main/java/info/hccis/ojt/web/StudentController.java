/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.web;

import info.hccis.ojt.dao.OjtDAO;
import info.hccis.ojt.data.springdatajpa.OjtReflectionRepository;

import info.hccis.ojt.entity.OjtReflection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author divya
 */
@Controller
public class StudentController {

    private final OjtReflectionRepository ojtRep;
    String countMsg = "";

    /**
     * This constructor will inject the Repository object into this controller.
     * It will allow this class' methods to use methods from this Spring JPA
     * repository object.
     *
     * @since 20191021
     * @author BJM
     * @param ur
     */
    @Autowired
    public StudentController(OjtReflectionRepository ojtRep) {
        this.ojtRep = ojtRep;

    }

    @RequestMapping("/ojt/add")
    public String studentAdd(Model model) {

        //put a Ojt reflection object in the model to be used to associate with the input tags of 
        //the form.
        OjtReflection newStudent = new OjtReflection();
        model.addAttribute("student", newStudent);

        //This will send the user to the welcome.html page.
        return "ojt/add";
    }

    @RequestMapping("/")
    public String studentShow(Model model) {

        //put a Ojt reflection object in the model to be used to associate with the input tags of 
        //the form.
        countMsg = "The Total Number of reflection " + ojtRep.count();
        model.addAttribute("totalReflection", countMsg);

        model.addAttribute("students", ojtRep.findAll());
        return "/ojt/list";

    }

    @RequestMapping("/ojt/update")
    public String studentUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println(" id passed=" + idToFind);
        //- It will go to the database and load the camper details into a camper
        //  object and put that object in the model.  
        //This will send the user to the welcome.html page.
        info.hccis.ojt.entity.OjtReflection editStudent = ojtRep.findOne(Integer.parseInt(request.getParameter("id")));
        //  OjtReflection editStudent = OjtDAO.select(Integer.parseInt(idToFind));

        model.addAttribute("student", editStudent);
        return "/ojt/add";

    }

    @RequestMapping("/ojt/delete")
    public String studentDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println(" id passed=" + idToFind);

        try {
            //- It will go to the database and load the student details into a OJTreflection
            //  object and put that object in the model.
            // OjtDAO.delete(Integer.parseInt(idToFind));
            ojtRep.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the students list so it can be shown on the next view.
        // model.addAttribute("students", OjtDAO.selectAll());
        countMsg = "The Total Number of reflection " + ojtRep.count();
        model.addAttribute("totalReflection", countMsg);
        model.addAttribute("students", ojtRep.findAll());
        return "/ojt/list";

    }

    @RequestMapping("/ojt/addSubmit")
    public String studentAddSubmit(Model model, @Valid @ModelAttribute("student") OjtReflection theStudentFromTheForm, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "ojt/add";
        }
        //Call the dao method to put this guy in the database.

        try {
            //OjtDAO.update(theStudentFromTheForm);
            info.hccis.ojt.entity.OjtReflection studentReflection = new info.hccis.ojt.entity.OjtReflection();
            studentReflection.setId(theStudentFromTheForm.getId());
            studentReflection.setStudentId(theStudentFromTheForm.getStudentId());
            studentReflection.setReflection(theStudentFromTheForm.getReflection());
            studentReflection.setStudentName(theStudentFromTheForm.getStudentName());
            ojtRep.save(studentReflection);
            //ojtRep.
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("-Did we get here?");
        //Reload the students list so it can be shown on the next view.

        countMsg = "The Total Number of reflection " + ojtRep.count();
        model.addAttribute("totalReflection", countMsg);
        model.addAttribute("students", ojtRep.findAll());

        //This will send the user to the welcome.html page.
        return "ojt/list";
    }

    /*@RequestMapping("/ojt/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("student") OjtReflection student, BindingResult result) {

      /*  if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "ojt/add";
        }

          try {
            OjtDAO.update(student);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("-Did we get here?");
        //Reload the students list so it can be shown on the next view.
        model.addAttribute("students", OjtDAO.selectAll());
        //This will send the user to the welcome.html page.
        return "ojt/list";

    }*/
}
